<?php include($_SERVER['DOCUMENT_ROOT'] . '/assets/include/header1.php'); ?>
<title>T.E.G</title>
<?php include($_SERVER['DOCUMENT_ROOT'] . '/assets/include/header2.php'); ?>
	<div class="c-maintitle1 c-maintitle1--teamshien">
		<h2>一般社団法人team shien<span>About</span></h2>
	</div>  <!-- /c-maintitle -->

	<div class="c-breadcrumbs">
		<div class="l-container">
			<ul class="c-path">
				<li><a href="#">HOME</a></li>
				<li>一般社団法人team shien</li>
			</ul>
		</div>
	</div><!-- /c-breadcrumbs -->


	<div class="p-teamshien">

		<div class="p-teamshien1">
			<div class="l-flame2">
				<div class="l-container">
					<div class="c-title4">
						介護保険ケアプラン（事業所番号 1371212207）
					</div>
					<h4 class="c-title5"><img src="../../assets/image/teamshien/icon-teamshien.png" alt="">team shien</h4>
					<div class="l-flame1">
							<div class="l-flame1__left">
								<p>看護師・社会福祉士・精神保健福祉士・介護福祉士の資格を持った福祉のスペシャリストが
								介護保険利用のためのケアプランを作成を承ります。
								</p>
								<p>介護保険の利用に関わらず、福祉医療全般のご相談を無料にて承ります。</p>
								<p class="c-info2">
									<span>お問い合わせ先</span>
									月～金曜日 9:00～18:00 / TEL.03-6379-4343
								</p>
							</div>
							<div class="l-flame1__right">
								<img src="../../assets/image/teamshien/img-teamshien1.png" alt="">
							</div>
					</div>
				</div>
			</div>
		</div>

		<div class="p-teamshien2">
			<div class="l-flame2 u-bg1">
				<div class="l-container">
					<div class="c-title4">
						訪問介護（事業所番号 1371209469
					</div>
					<h4 class="c-title5"><img src="../../assets/image/teamshien/icon-teamshien.png" alt="">ちーむしえんあどぼかしー</h4>
					<div class="l-flame1">
							<div class="l-flame1__left">
								<p>介護員がお宅を訪問してサービスを提供させて頂きます。介護保険外の自費サービスも行っております。<br>
									まずは、お気軽にお問い合わせください。
								</p>
								<p class="c-info2 c-info2--color2">
									<span>お問い合わせ先</span>
									月～金曜日 9:00～18:00 / TEL.03-6379-4343
								</p>
							</div>
							<div class="l-flame1__right">
								<img src="../../assets/image/teamshien/img-teamshien2.png" alt="">
							</div>
					</div>
				</div>
			</div>
		</div>

		<div class="p-teamshien3">
			<div class="l-flame2">
				<div class="l-container">
					<div class="c-title4">
						一般相談・特定相談支援（事業所番号 1331202471）
					</div>
					<h4 class="c-title5"><img src="../../assets/image/teamshien/icon-teamshien.png" alt="">team shien m.a</h4>
					<div class="l-flame1">
							<div class="l-flame1__left">
								<p class="c-title6">地域移行<br>地域定着</p>
								<p>障害をお持ちの方で、長期入院・入所をなさっている方の地域生活への移行・定着をお手伝いしています。<br>
								※地域の行政担当・病院・施設の相談員の方を通じてお問い合わせください。
								</p>
								<p>障害福祉サービスの利用を希望されている方のサービス等利用計画作成を承ります。<br>
								例）訪問介護の利用や、就労支援先への通所など</p>
								
							</div>
							<div class="l-flame1__right">
								<img src="../../assets/image/teamshien/img-teamshien3.png" alt="">
							</div>
					</div>
				</div>
			</div>
		</div>

		<div class="p-teamshien4">
			<div class="l-flame2 u-bg1">
				<div class="l-container">
					<div class="c-title4">
						居宅介護・移動支援（事業所番号 1311202467）
					</div>
					<h4 class="c-title5"><img src="../../assets/image/teamshien/icon-teamshien.png" alt="">team shien m.a</h4>
					<div class="l-flame1">
							<div class="l-flame1__left">
								<p>ちーむしえんあどぼかしーの訪問介護同様、専門の知識を有した介護員がケアに伺います。<br>まずは、お気軽にお問い合わせください。
								</p>
								<p>障害福祉サービス受給者証をお持ちの方は、介護給付費の１割負担でご利用いただけます。<br>（所得に応じて負担上限金額が異なります）</p>
								
							</div>
							<div class="l-flame1__right">
								<img src="../../assets/image/teamshien/img-teamshien4.png" alt="">
							</div>
					</div>
				</div>
			</div>
		</div>

		<div class="p-teamshien5">
			<div class="l-flame2">
				<div class="l-container">
					<div class="c-title4">
						一般相談・特定相談支援（事業所番号 1331202471）
					</div>
					<h4 class="c-title5"><img src="../../assets/image/teamshien/icon-teamshien.png" alt="">ちーむしえん 研修部</h4>
					<div class="l-flame1">
							<div class="l-flame1__left">
							<p class="c-title6">介護職員初任者研修<br>同行援護従事者研修</p>
								<p>社会人でも取得しやすいプログラム、通信制を導入しております。<br>
								当社講師が誠心誠意、次世代の福祉人材を育て、社会へ貢献いたします。<br>
								受講される方の意向を反映したオーダーメイド制研修を実施。<br>
								6名以上の人数が揃い次第、随時研修を開始いたします。<br>
								</p>
								<p class="c-info2">
									<span>お問い合わせ先</span>
									月～金曜日 9:00～18:00 / TEL.03-6379-4343
								</p>
							</div>
							<div class="l-flame1__right">
								<img src="../../assets/image/teamshien/img-teamshien5.png" alt="">
							</div>
					</div>
				</div>
			</div>
		</div>

		<div class="p-teamshien6 u-bg1">
			<div class="l-flame2">
					<div class="l-container">
					<div class="c-title4">
							お問い合わせ先
						</div>

						<dl class="c-table1 c-table1--color2">
						<dt>電話番号</dt>
						<dd>03-6379-5484</dd>
						<dt>メールアドレス</dt>
						<dd>irishcafe@ab.auone-net.jp</dd>
						
					</dl>
				</div>	
			</div>
		</div>

		<div class="p-teamshien7">
			<div class="l-flame2">
				<div class="l-container">
					<div class="c-title4">
							求人案内
					</div>
					<div class="p-teamshien7__content">
						<p>随時、スタッフを募集しております。詳細については、お電話にてお尋ねください。</p>
						<p>【募集職種】<br>
							①ホームヘルパー<br>
							②ケアマネージャー<br>
							③相談支援専門員</p>
					</div>
				</div>	
			</div>
		</div>

		<div class="p-teamshien8">
			<div class="l-flame2 u-bg1">
				<div class="l-container">
					<div class="c-title4">
							アクセスマップ
					</div>
					<div class="c-mapBlock">
						<div class="c-map1">
							<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3241.4152130342773!2d139.6573658155701!3d35.66677693837128!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x6018f30e11f17561%3A0x9bb452e4b9254772!2s1+Chome-13-9+Hanegi%2C+Setagaya-ku%2C+T%C5%8Dky%C5%8D-to+156-0042%2C+Japan!5e0!3m2!1sen!2s!4v1503037614438" frameborder="0" style="border:0" allowfullscreen=""></iframe>
						</div>
						<a href="https://maps.google.com/maps?ll=35.666773,139.659555&amp;z=16&amp;t=m&amp;hl=ja&amp;gl=US&amp;mapclient=embed&amp;q=%E3%80%92156-0042%20%E6%9D%B1%E4%BA%AC%E9%83%BD%E4%B8%96%E7%94%B0%E8%B0%B7%E5%8C%BA%E7%BE%BD%E6%A0%B9%E6%9C%A8%EF%BC%91%E4%B8%81%E7%9B%AE%EF%BC%91%EF%BC%93%E2%88%92%EF%BC%99%20%E6%97%A5%E6%9C%AC" target="_blank" class="c-map1-detail">詳しい地図を見る</a>
						<div class="c-list1">
							<p><img src="../../assets/image/common/icon-car.png" alt="">京王線「代田橋」駅　徒歩6分</p>
							<p><img src="../../assets/image/common/icon-car.png" alt="">京王井の頭線「新代田」駅　徒歩9分</p>
							<p><img src="../../assets/image/common/icon-car.png" alt="">京王井の頭線「東松原」駅   徒歩9分　</p>
						</div>
						<p class="map-note">※家庭料理のおざわが一階にあります。</p>
					</div>
				</div>
			</div>
		</div>


	</div>

<?php include($_SERVER['DOCUMENT_ROOT'] . '/assets/include/footer.php'); ?>
