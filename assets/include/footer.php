<footer id="footer">

	<div class="c-footer1">
		<div class="l-container">
			<ul class="menu-footer">
				<li><a href="../../top/index.php">ホーム</a></li>
				<li><a href="../../news/index.php">お知らせ</a></li>
				<li><a href="../../teamsakata/index.php">株式会社team sakata</a></li>
				<li><a href="../../shien/index.php">一般社団法人SHIEN </a></li>
				<li><a href="../../teamshien/index.php">一般社団法人team shien</a></li>
				<li><a href="../../sakura/index.php">NPO法人ライフサポートさくら </a></li>
				<li><a href="../../company/index.php">会社案内</a></li>
			</ul>
		</div>
	</div>
	<div class="c-footer2">
		<div class="l-container">
			<div class="c-footer2-content">
				<div class="footer-info">
					<p class="footer-info__title">株式会社team sakata、一般社団法人SHIEN、一般社団法人team shien </p>
					<p>〒156-0042　東京都世田谷区羽根木1-13-9-201</p>
					<p>TEL：03-6379-5484（代表）</p>
				</div>
				<div class="footer-info">
					<p class="footer-info__title">NPO法人ライフサポートさくら</p>
					<p>〒154-0021　東京都世田谷区豪徳寺1-21-5 ヴィラ・ゴートク1-E</p>
					<p>TEL：03-3439-3977</p>
				</div>
			</div>
		</div>
	</div>
	<div class="c-footer3">
		<div class="l-container">
			COPYRIGHT© Team Egao Group　 ALL RIGHTS RESERVED.
		</div>
	</div>
	<button class="c-gotop"><a href="#" title=""><img src="../../assets/image/common/top-page.png" alt=""></a></button>
</footer>


</body>
</html>