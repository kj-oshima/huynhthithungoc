</head>
<body>
<header id="header" class="">
	
	<div class="c-header1">
		<div class="l-container">
			有資格者がご自宅に訪問し、身体介護や生活援助をおこなうサービスを提供しております。その他福祉と医療に関するご相談も無料で承ります。
		</div>
	</div>

	<div class="l-container">
		<div class="c-header2">
			<div class="c-header2__left">
				<img src="../../assets/image/common/logo.png" alt="">
			</div>
			<div class="c-header2__right">
				<div class="header2-contact-text">
					お気軽にお問い合わせください。
				</div>
				<div class="header2-contact-phone">
					<img src="../../assets/image/common/header-text.png" alt="">
				</div>
			</div>
		</div>
	</div>

	<div class="c-gnav">
		<div class="l-container">
			<ul class="header-menu">
				<li class="header-menu__item"><a href="../../top/index.php" id="center-a"><img src="../../assets/image/common/icon-home.png" alt=""></a></li>
				<li class="header-menu__item"><a href="../../news/index.php" id="center-a">お知らせ</a></li>
				<li class="header-menu__item"><a href="../../teamsakata/index.php" >株式会社<br><span>team sakata</span></a></li>
				<li class="header-menu__item"><a href="../../shien/index.php" >一般社団法人<br><span>SHIEN</span></a></li>
				<li class="header-menu__item"><a href="../../teamshien/index.php" >一般社団法人<br><span>team shien</span></a></li>
				<li class="header-menu__item"><a href="../../sakura/index.php" >NPO法人<br><span>ライフサポートさくら</span></a></li>
				<li class="header-menu__item"><a href="../../company/index.php" id="center-a">会社案内</a></li>
			</ul>
		</div>
	</div>

</header><!-- /header -->