<?php include($_SERVER['DOCUMENT_ROOT'] . '/assets/include/header1.php'); ?>
<title>T.E.G</title>
<?php include($_SERVER['DOCUMENT_ROOT'] . '/assets/include/header2.php'); ?>
	<div class="c-maintitle1 c-maintitle1--shien">
		<h2>一般社団法人SHIEN<span>About</span></h2>
	</div>  <!-- /c-maintitle -->

	<div class="c-breadcrumbs">
		<div class="l-container">
			<ul class="c-path">
				<li><a href="#">HOME</a></li>
				<li>一般社団法人SHIEN</li>
			</ul>
		</div>
	</div><!-- /c-breadcrumbs -->

	<div class="p-shien p-content1">
		
		<div class="p-shien1">
			<div class="l-flame2">
				<div class="l-container">
				<div class="c-title4">
					成年後見
				</div>
				<h4 class="c-title5"><img src="../../assets/image/teamshien/icon-teamshien.png" alt="">成年後見制度の利用相談・支援</h4>
				<div class="l-flame1">
						<div class="l-flame1__left">
							<p class="c-title6">成年後見制度とは</p>
							<p>認知症や知的障がい、精神障がいなどによって判断能力が不十分な方々は、預貯金の管理や遺産分割などの法律行為（財産管理）や、福祉サービスの利用など、日常生活での様々な契約（身上監護）などを行うことが困難であったり、悪徳商法などの被害にあうおそれがあります。成年後見制度は、このような判断能力の不十分な方々を法律的に保護し、支援する制度です。</p>
							<p class="c-title6">相談・支援</p>
							<p>電話や窓口で、成年後見制度に関する相談や成年後見制度を利用するための手続きや申立て、費用などについて詳しく説明し、申し立て手続き等の支援をします。</p>
						</div>
						<div class="l-flame1__right">
							<img src="../../assets/image/shien/img-shien1.png" alt="">
						</div>
				</div>
			</div>
			</div>
		</div>

		<div class="p-shien2 u-bg1">
			<div class="l-flame2">
				<div class="l-container">
				<div class="c-title4">
					成年後見
				</div>
				<h4 class="c-title5"><img src="../../assets/image/teamshien/icon-teamshien.png" alt="">成年後見制度の利用相談・支援</h4>
				<div class="l-flame1">
						<div class="l-flame1__left">
							<p>SHIEN治療院では、ご利用者様の症状やご体調に合わせて、機能回復および機能維持を目的とした訪問医療マッサージを行っております。<br>また、心のふれあいを大切にして、笑顔あふれる施術を心がけております。</p>
							<p>医療保険でマッサージが受けられることは、ご存じでしょうか？</p>
							<p>SHIEN治療院が提供する訪問医療マッサージは、歩行困難な方を対象に医師の同意（同意書）に基づき、医療保険が適用できます。<br>
							保険が適用となりますと、後期高齢者健康保険の方は実費の1割、その他の保険の方は実費の3割、障害1級2級（医療費受給者証）持参の方は全額公費負担となり、少ない自己負担でマッサージを受けていただくことができます（往療料にも保険が適用されます）。 </p>
						</div>
						<div class="l-flame1__right">
							<img src="../../assets/image/shien/img-shien2.png" alt="">
						</div>
				</div>
			</div>
			</div>
		</div>

		<div class="p-shien3">
			<div class="l-flame2">
				<div class="l-container">
				<div class="c-title4">
						お問い合わせ先
					</div>

					<dl class="c-table1">
						<dt>電話番号</dt>
						<dd>03-3323-2495</dd>
					</dl>
			</div>
			</div>
		</div>

		<div class="p-shien4 u-bg1">
			<div class="l-flame2">
				<div class="l-container">
				<div class="c-title4">
						求人案内
					</div>
					<div class="c-text1">
						<p>テキストを入力することができます。テキストを入力することができます。テキストを入力することができます。テキストを入力することができます。テキストを入力することができます。テキストを入力することができます。テキストを入力することができます。テキストを入力することができます。テキストを入力することができます。テキストを入力することができます。テキストを入力することができます。テキストを入力することができます。テキストを入力することができます。テキストを入力することができます。テキストを入力することができます。テキストを入力することができます。テキストを入力することができます。テキストを入力することができます。テキストを入力することができます。テキストを入力することができます。テキストを入力することができます。テキストを入力することができます。</p>
					</div>
			</div>
			</div>
		</div>

		<div class="p-shien5">
			<div class="l-flame2">
				<div class="l-container">
				<div class="c-title4">
						アクセスマップ
					</div>
					<div class="c-mapBlock">
						<div class="c-map1">
							<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3241.4152130342773!2d139.6573658155701!3d35.66677693837128!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x6018f30e11f17561%3A0x9bb452e4b9254772!2s1+Chome-13-9+Hanegi%2C+Setagaya-ku%2C+T%C5%8Dky%C5%8D-to+156-0042%2C+Japan!5e0!3m2!1sen!2s!4v1503037614438" frameborder="0" style="border:0" allowfullscreen=""></iframe>
						</div>
						<a href="https://maps.google.com/maps?ll=35.666773,139.659555&amp;z=16&amp;t=m&amp;hl=ja&amp;gl=US&amp;mapclient=embed&amp;q=%E3%80%92156-0042%20%E6%9D%B1%E4%BA%AC%E9%83%BD%E4%B8%96%E7%94%B0%E8%B0%B7%E5%8C%BA%E7%BE%BD%E6%A0%B9%E6%9C%A8%EF%BC%91%E4%B8%81%E7%9B%AE%EF%BC%91%EF%BC%93%E2%88%92%EF%BC%99%20%E6%97%A5%E6%9C%AC" target="_blank" class="c-map1-detail">詳しい地図を見る</a>
						<div class="c-list1">
							<p><img src="../../assets/image/common/icon-car.png" alt="">京王線「代田橋」駅　徒歩6分</p>
							<p><img src="../../assets/image/common/icon-car.png" alt="">京王井の頭線「新代田」駅　徒歩9分</p>
							<p><img src="../../assets/image/common/icon-car.png" alt="">京王井の頭線「東松原」駅   徒歩9分　</p>
						</div>
						<p class="map-note">※家庭料理のおざわが一階にあります。</p>
					</div>
			</div>
			</div>
		</div>

	</div>

<?php include($_SERVER['DOCUMENT_ROOT'] . '/assets/include/footer.php'); ?>
