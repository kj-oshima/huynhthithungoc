<?php include($_SERVER['DOCUMENT_ROOT'] . '/assets/include/header1.php'); ?>
<title>T.E.G</title>
<?php include($_SERVER['DOCUMENT_ROOT'] . '/assets/include/header2.php'); ?>

<div class="c-maintitle1 c-maintitle1--new">
	<h2>お知らせ<span>News</span></h2>
</div>
<br>
<br>
<div class="c-breadcrumbs">
	<div class="l-container">
		<ul class="c-path">
			<li><a href="#">HOME</a></li>
			<li>お知らせ</li>
		</ul>
	</div>
</div>
<br>
<br>
<div class="c-mainvisual">
	<img src="../../assets/image/common/img-index.png" alt="">
</div>
<br>
<br>
<div class="l-container">

	<div class="c-title1">
		チームエガオグループ（T.E.G Team Egao Group）
		<span>About</span>
		
	</div>

	<br>
	<br>

	<div class="c-title2">
		ホームページをリニューアルしました。
	</div>
	<br>
	<br>
	<div class="c-title3">
		<img src="../../assets/image/company/company-1.png" alt="">株式会社team sakata
	</div>
	<br>
	<br>
	<div class="c-title3">
		<img src="../../assets/image/company/company-2.png" alt="">一般社団法人SHIEN
	</div>
	<br>
	<br>
	<div class="c-title3">
		<img src="../../assets/image/company/company-3.png" alt="">一般社団法人team shien
	</div>
	<br>
	<br>
	<div class="c-title3">
		<img src="../../assets/image/company/company-4.png" alt="">NPO法人ライフサポートさくら
	</div>
	<br>
	<br>
	<div class="c-title4">
		介護保険ケアプラン（事業所番号 1371212207）
	</div>
	<br>
	<br>
	<h4 class="c-title5"><img src="../../assets/image/teamshien/icon-teamshien.png" alt="">team shien</h4>
	<br>
	<br>
	<p class="c-title6">成年後見制度とは</p>
	<br>
	<br>
	<p class="c-title7">YYYY/MM/DD</p>
	<br>
	<br>
	<div class="c-title100">
		<div class="c-title1">
			お知らせ
			<span>News</span>
		</div>
		<button class="c-btn1">
		<a href="#">Facebook</a>
		</button>
	</div>
	<br>
	<br>
	<dl class="c-table1">
		<dt>運営主体</dt>
		<dd>株式会社team sakata</dd>
		<dt>所在地</dt>
		<dd>〒156-0042　東京都世田谷区羽根木1-13-9-201</dd>
		<dt>代表者名</dt>
		<dd>阪田　祐治</dd>
		<dt>TEL</dt>
		<dd>03-3323-2495</dd>
		<dt>FAX</dt>
		<dd>03-3327-7166</dd>
		<dt>設立</dt>
		<dd></dd>
	</dl>
	<br>
	<br>
	<dl class="c-table1 c-table1--color2">
		<dt>電話番号</dt>
		<dd>03-6379-5484</dd>
		<dt>メールアドレス</dt>
		<dd>irishcafe@ab.auone-net.jp</dd>
		
	</dl>
	<br>
	<br>
	<div class="c-list1">
		<p><img src="../../assets/image/common/icon-car.png" alt="">京王線「代田橋」駅　徒歩6分</p>
		<p><img src="../../assets/image/common/icon-car.png" alt="">京王井の頭線「新代田」駅　徒歩9分</p>
		<p><img src="../../assets/image/common/icon-car.png" alt="">京王井の頭線「東松原」駅   徒歩9分　</p>
	</div>
	<br>
	<br>
	<ul class="c-list2">
		<li>
			<p class="c-list2__date">2017/11/25 </p>
			<p class="c-list2__text">イベント情報ページを更新しました。</p>
		</li>
		<li>
			<p class="c-list2__date">2017/10/20</p>
			<p class="c-list2__text">イベント情報ページを更新しました。</p>
		</li>
		<li>
			<p class="c-list2__date">2017/09/25 </p>
			<p class="c-list2__text">ホームページを開設いたしました。今後とも、チームエガオグループをよろしくお願いいたします。</p>
		</li>
	</ul>
	<br>
	<br>
	<div class="c-info1">
		<h4 class="c-info1__title">このような法人様におすすめ</h4>
		<ul class="c-info1__list">
			<li>介護事業の事が全くわからない。</li>
			<li>一人では不安だから一緒に事業計画から安定運営までサポートして欲しい。</li>
			<li>障害福祉サービスを取り組みたい。</li>
			<li>更生保護事業に興味がある。</li>
		</ul>
	</div>
	<br>
	<br>
	<p class="c-info2">
		<span>お問い合わせ先</span>
		月～金曜日 9:00～18:00 / TEL.03-6379-4343
	</p>
	<br>
	<br>
	<p class="c-info2 c-info2--color2">
		<span>お問い合わせ先</span>
		月～金曜日 9:00～18:00 / TEL.03-6379-5484
	</p>
	<br>
	<br>
	<div class="c-info3">
		<div class="c-info3__left">
			<p class="c-info3__left-text1">チームエガオグループで一緒に働きませんか？</p>
			<p class="c-info3__left-text2">ご興味のある方はお電話にてご連絡ください。</p>
			<img src="../../assets/image/common/img-info3.png" alt="">
		</div>
		<div class="c-info3__right">
			<ul>
				<li><a href="#"><span><img src="../../assets/image/top/icon-top1.png" alt=""></span>株式会社team sakata</a></li>
				<li><a href="#"><span><img src="../../assets/image/top/icon-top2.png" alt=""></span>一般社団法人SHIEN</a></li>
				<li><a href="#"><span><img src="../../assets/image/top/icon-top3.png" alt=""></span>一般社団法人team shien</a></li>
				<li><a href="#"><span><img src="../../assets/image/top/icon-top4.png" alt=""></span>NPO法人 ライフサポートさくら</a></li>
			</ul>
		</div>
	</div>
	<br>
	<br>
	<div class="c-map1">
		<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3241.4152130342773!2d139.6573658155701!3d35.66677693837128!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x6018f30e11f17561%3A0x9bb452e4b9254772!2s1+Chome-13-9+Hanegi%2C+Setagaya-ku%2C+T%C5%8Dky%C5%8D-to+156-0042%2C+Japan!5e0!3m2!1sen!2s!4v1503037614438" frameborder="0" style="border:0" allowfullscreen=""></iframe>
	</div>
	<br>
	<br>
	<div class="c-mapBlock">
		<div class="c-map1">
			<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3241.4152130342773!2d139.6573658155701!3d35.66677693837128!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x6018f30e11f17561%3A0x9bb452e4b9254772!2s1+Chome-13-9+Hanegi%2C+Setagaya-ku%2C+T%C5%8Dky%C5%8D-to+156-0042%2C+Japan!5e0!3m2!1sen!2s!4v1503037614438" frameborder="0" style="border:0" allowfullscreen=""></iframe>
		</div>
		<a href="https://maps.google.com/maps?ll=35.666773,139.659555&amp;z=16&amp;t=m&amp;hl=ja&amp;gl=US&amp;mapclient=embed&amp;q=%E3%80%92156-0042%20%E6%9D%B1%E4%BA%AC%E9%83%BD%E4%B8%96%E7%94%B0%E8%B0%B7%E5%8C%BA%E7%BE%BD%E6%A0%B9%E6%9C%A8%EF%BC%91%E4%B8%81%E7%9B%AE%EF%BC%91%EF%BC%93%E2%88%92%EF%BC%99%20%E6%97%A5%E6%9C%AC" target="_blank" class="c-map1-detail">詳しい地図を見る</a>
		<div class="c-list1">
			<p><img src="../../assets/image/common/icon-car.png" alt="">京王線「代田橋」駅　徒歩6分</p>
			<p><img src="../../assets/image/common/icon-car.png" alt="">京王井の頭線「新代田」駅　徒歩9分</p>
			<p><img src="../../assets/image/common/icon-car.png" alt="">京王井の頭線「東松原」駅   徒歩9分　</p>
		</div>
		<p class="map-note">※家庭料理のおざわが一階にあります。</p>
	</div>
	<br>
	<br>
	<ul class="c-nav1">
		<li><a href="#" >株式会社team sakata</a></li>
		<li><a href="#" >一般社団法人SHIEN</a></li>
		<li><a href="#" >一般社団法人team shien</a></li>
		<li><a href="#" >NPO法人ライフサポートさくら</a></li>
	</ul>
	<br>
	<br>
	<div class="c-nav2">
		<p><a href="#"><img src="../../assets/image/top/img-top1.png" alt=""></a></p>
		<p><a href="#"><img src="../../assets/image/top/img-top2.png" alt=""></a></p>
		<p><a href="#"><img src="../../assets/image/top/img-top3.png" alt=""></a></p>
		<p><a href="#"><img src="../../assets/image/top/img-top4.png" alt=""></a></p>
	</div>
	<br>
	<br>
	<button class="c-gotop"><a href="#" title=""><img src="../../assets/image/common/top-page.png" alt=""></a></button>
	<br>
	<br>
	
	<button class="c-btn1">
		<a href="#">Facebook</a>
	</button>
	
	<br>
	<br>
	<br>
	<br>
	<div class="c-text1">
		<p>テキストを入力することができます。テキストを入力することができます。テキストを入力することができます。テキストを入力することができます。テキストを入力することができます。テキストを入力することができます。テキストを入力することができます。テキストを入力することができます。テキストを入力することができます。テキストを入力することができます。テキストを入力することができます。テキストを入力することができます。テキストを入力することができます。テキストを入力することができます。テキストを入力することができます。テキストを入力することができます。テキストを入力することができます。テキストを入力することができます。テキストを入力することができます。テキストを入力することができます。テキストを入力することができます。テキストを入力することができます。</p>
	</div>
	<br>
	<br>
	<br>
	<br>
	<div class="c-entry1">
		<div class="c-entry1__text">
			<p>本文を入力します。お客様自身で、編集・更新することができます。</p><p>画像を1枚挿入できます。</p>
		</div>
		<div class="c-entry1__img">
			<p>イメージ画像</p>
		</div>
	</div>
	<br>
	<br>
	<br>
	<br>
	<div class="l-flame1">
		<div class="l-flame1__left">
			<p>看護師・社会福祉士・精神保健福祉士・介護福祉士の資格を持った福祉のスペシャリストが
			介護保険利用のためのケアプランを作成を承ります。
			</p>
			<p>介護保険の利用に関わらず、福祉医療全般のご相談を無料にて承ります。</p>
			<p class="c-info2">
				<span>お問い合わせ先</span>
				月～金曜日 9:00～18:00 / TEL.03-6379-4343
			</p>
		</div>
		<div class="l-flame1__right">
			<img src="assets/image/teamshien/img-teamshien1.png" alt="">
		</div>
	</div>
	<br>
	<br>
	<br>
	<br>
</div>


<?php include($_SERVER['DOCUMENT_ROOT'] . '/assets/include/footer.php'); ?>