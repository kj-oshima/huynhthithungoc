<?php include($_SERVER['DOCUMENT_ROOT'] . '/assets/include/header1.php'); ?>
<title>T.E.G</title>
<?php include($_SERVER['DOCUMENT_ROOT'] . '/assets/include/header2.php'); ?>
	<div class="c-maintitle1 c-maintitle1--sakura">
		<h2>NPO法人ライフサポートさくら<span>About</span></h2>
	</div>  <!-- /c-maintitle -->

	<div class="c-breadcrumbs">
		<div class="l-container">
			<ul class="c-path">
				<li><a href="#">HOME</a></li>
				<li>NPO法人ライフサポートさくら</li>
			</ul>
		</div>
	</div><!-- /c-breadcrumbs -->

	<div class="p-sakura p-content1">
		
		<div class="p-sakura1">
			<div class="l-container">
				<div class="c-title4">
					<img src="../../assets/image/sakura/title-sakura.png" alt="">
				</div>
				<h4 class="c-title5"><img src="../../assets/image/teamshien/icon-teamshien.png" alt="">地域デイサービス　（　さくらとゆかいな仲間たち　）</h4>
				<div class="l-flame1">
						<div class="l-flame1__left">
							<p>みなさんで一緒にごはんを作って食べて、おしゃべりやちょっとした運動をして楽しい時間を一緒に過ごしませんか？
							</p>
							<p><span class="c-title6">参加対象者</span><br>要支援１・２及びチェックリスト該当者（65歳以上）</p>
							<p><span class="c-title6">参加料</span><br>食事代：500円（イベントの材料費別途徴収）</p>
							<p class="c-info2">
								<span>お問い合わせ先</span>
								月～金曜日 9:00～18:00 / TEL.03-6379-4343
							</p>
						</div>
						<div class="l-flame1__right">
							<img src="../../assets/image/sakura/img-sakura1.png" alt="">
						</div>
					</div>
			</div>
		</div>  <!-- /p-sakura1 -->

		<div class="p-sakura2 u-bg1">
			<div class="l-container">
				<div class="c-title4">
					ノーマライゼーションギャラリー
				</div>
				<h4 class="c-title5"><img src="../../assets/image/teamshien/icon-teamshien.png" alt="">team さくら</h4>
				<div class="l-flame1">
						<div class="l-flame1__left">
							<p>障害の有無を意識しなくても生活ができる社会の実現を目指す考え方を共有するフリースペースです。
							</p>
						</div>
						<div class="l-flame1__right">
							<img src="../../assets/image/sakura/img-sakura2.png" alt="">
						</div>
					</div>
			</div>
		</div>  <!-- /p-sakura2 -->

		<div class="p-sakura3">
			<div class="l-flame2">
				<div class="l-container">
					<div class="c-title4">
						お問い合わせ先
					</div>
					<dl class="c-table1">
						<dt>電話番号</dt>
						<dd>03-3323-2495</dd>
						<dt>メールアドレス</dt>
						<dd>team-sakata@w6.dion.ne.jp</dd>
					</dl>
				</div>
			</div>
		</div>  <!-- /p-sakura3-->

		<div class="p-sakura4 u-bg1">
			<div class="l-flame2">
				<div class="l-container">
					<div class="c-title4">
						求人案内
					</div>
					<div class="c-text1">
						<p>テキストを入力することができます。テキストを入力することができます。テキストを入力することができます。テキストを入力することができます。テキストを入力することができます。テキストを入力することができます。テキストを入力することができます。テキストを入力することができます。テキストを入力することができます。テキストを入力することができます。テキストを入力することができます。テキストを入力することができます。テキストを入力することができます。テキストを入力することができます。テキストを入力することができます。テキストを入力することができます。テキストを入力することができます。テキストを入力することができます。テキストを入力することができます。テキストを入力することができます。テキストを入力することができます。テキストを入力することができます。</p>
					</div>
				</div>
			</div>
		</div>  <!-- /p-sakura1 -->

		<div class="p-sakura5">
			<div class="l-flame2">
				<div class="l-container">
					<div class="c-title4">
						アクセスマップ
					</div>
					<div class="c-mapBlock">
						<div class="c-map1">
							<iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d2524.089868803704!2d139.64780339885021!3d35.652909161333206!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x6018f39e89d04f03%3A0xee26a0f25f9b58ab!2z5pel5pys44CB44CSMTU0LTAwMjEg5p2x5Lqs6YO95LiW55Sw6LC35Yy66LGq5b6z5a-677yR5LiB55uu77yS77yR4oiS77yVIOODtOOCo-ODqeOCtOODvOODiOOCrw!5e0!3m2!1sja!2sus!4v1503993533806" frameborder="0" style="border:0" allowfullscreen=""></iframe>
						</div>
						<a href="https://www.google.com/maps?ll=35.652961,139.647727&amp;z=18&amp;t=m&amp;hl=ja&amp;gl=US&amp;mapclient=embed&amp;q=%E3%83%B4%E3%82%A3%E3%83%A9%E3%82%B4%E3%83%BC%E3%83%88%E3%82%AF+%E6%97%A5%E6%9C%AC+%E3%80%92154-0021+%E6%9D%B1%E4%BA%AC%E9%83%BD%E4%B8%96%E7%94%B0%E8%B0%B7%E5%8C%BA%E8%B1%AA%E5%BE%B3%E5%AF%BA%EF%BC%91%E4%B8%81%E7%9B%AE%EF%BC%92%EF%BC%91%E2%88%92%EF%BC%95" target="_blank" class="c-map1-detail">詳しい地図を見る</a>
						<div class="c-list1">
							<p><img src="../../assets/image/common/icon-car.png" alt="">小田急小田原線「豪徳寺」駅　徒歩2分</p>
							<p><img src="../../assets/image/common/icon-car.png" alt="">東急世田谷線「山下」駅　徒歩3分</p>
						</div>
					</div>
				</div>
			</div>
		</div>  <!-- /p-sakura5 -->

	</div>

<?php include($_SERVER['DOCUMENT_ROOT'] . '/assets/include/footer.php'); ?>
