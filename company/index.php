<?php include($_SERVER['DOCUMENT_ROOT'] . '/assets/include/header1.php'); ?>
<title>T.E.G</title>
<?php include($_SERVER['DOCUMENT_ROOT'] . '/assets/include/header2.php'); ?>
	<div class="c-maintitle1 c-maintitle1--companny">
		<h2>会社案内<span>Company Data</span></h2>
	</div>

	<div class="c-breadcrumbs">
		<div class="l-container">
			<ul class="c-path">
				<li><a href="#">HOME</a></li>
				<li>会社案内</li>
			</ul>
		</div>
	</div>

	<div class="p-company">
		
		<div class="p-company1">
			<div class="l-container">
				<ul class="c-nav1">
					<li><a href="#" >株式会社team sakata</a></li>
					<li><a href="#" >一般社団法人SHIEN</a></li>
					<li><a href="#" >一般社団法人team shien</a></li>
					<li><a href="#" >NPO法人ライフサポートさくら</a></li>
				</ul>
			</div>
		</div>

		<div class="p-company2">
			<div class="l-container">
				<section>
					<div class="c-title3">
						<img src="../../assets/image/company/company-1.png" alt="">株式会社team sakata
					</div>
					<dl class="c-table1">
						<dt>運営主体</dt>
						<dd>株式会社team sakata</dd>
						<dt>所在地</dt>
						<dd>〒156-0042　東京都世田谷区羽根木1-13-9-201</dd>
						<dt>代表者名</dt>
						<dd>阪田　祐治</dd>
						<dt>TEL</dt>
						<dd>03-3323-2495</dd>
						<dt>FAX</dt>
						<dd>03-3327-7166</dd>
						<dt>設立</dt>
						<dd></dd>
					</dl>
				</section>

				<section>
					<div class="c-title3">
						<img src="../../assets/image/company/company-2.png" alt="">一般社団法人SHIEN
					</div>
					<dl class="c-table1">
						<dt>運営主体</dt>
						<dd>一般社団法人SHIEN</dd>
						<dt>所在地</dt>
						<dd>〒156-0042　東京都世田谷区羽根木1-13-9-201</dd>
						<dt>代表者名</dt>
						<dd>阪田　祐治</dd>
						<dt>TEL</dt>
						<dd>03-3323-2495</dd>
						<dt>FAX</dt>
						<dd>03-3327-7166</dd>
						<dt>設立</dt>
						<dd></dd>
					</dl>
				</section>

				<section>
					<div class="c-title3">
						<img src="../../assets/image/company/company-3.png" alt="">一般社団法人team shien
					</div>
					<dl class="c-table1">
						<dt>運営主体</dt>
						<dd>一般社団法人team shien</dd>
						<dt>所在地</dt>
						<dd>〒156-0042　東京都世田谷区羽根木1-13-9-201</dd>
						<dt>代表者名</dt>
						<dd>南大路　直子</dd>
						<dt>TEL</dt>
						<dd>03-6379-5484</dd>
						<dt>FAX</dt>
						<dd>03-3327-7166</dd>
						<dt>設立</dt>
						<dd></dd>
					</dl>
				</section>
				
				<section>
					<div class="c-title3">
						<img src="../../assets/image/company/company-4.png" alt="">NPO法人ライフサポートさくら
					</div>
					<dl class="c-table1">
						<dt>運営主体</dt>
						<dd>NPO法人ライフサポートさくら</dd>
						<dt>所在地</dt>
						<dd>〒154-0021　東京都世田谷区豪徳寺1-21-5 ヴィラ・ゴートク1-E</dd>
						<dt>代表者名</dt>
						<dd>阪田　祐治</dd>
						<dt>TEL</dt>
						<dd>03-3439-3977</dd>
						<dt>FAX</dt>
						<dd>03-3439-3978</dd>
						<dt>設立</dt>
						<dd></dd>
					</dl>
				</section>
				
			</div>
		</div>

	</div>

<?php include($_SERVER['DOCUMENT_ROOT'] . '/assets/include/footer.php'); ?>
