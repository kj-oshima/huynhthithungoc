<?php include($_SERVER['DOCUMENT_ROOT'] . '/assets/include/header1.php'); ?>
<title>T.E.G</title>
<?php include($_SERVER['DOCUMENT_ROOT'] . '/assets/include/header2.php'); ?>
	<div class="c-maintitle1 c-maintitle1--teamsakata">
		<h2>株式会社team sakata<span>About</span></h2>
	</div>  <!-- /c-maintitle -->

	<div class="c-breadcrumbs">
		<div class="l-container">
			<ul class="c-path">
				<li><a href="#">HOME</a></li>
				<li>株式会社team sakata</li>
			</ul>
		</div>
	</div><!-- /c-breadcrumbs -->

	<div class="p-teamsakata p-content1">

			<div class="p-teamsakata1">
				<div class="l-container">
					<div class="c-title4">
					総合社会福祉プロデュース
					</div>
					<h4 class="c-title5"><img src="../../assets/image/teamshien/icon-teamshien.png" alt="">事務所設立～運営計画</h4>
					<div class="l-flame1">
						<div class="l-flame1__left">
							<p>事業計画の立案から、採用、育成、利用者の獲得まで、事業が安定稼働までをサポートいたします。異業種・未経験からの参入でも、しっかりとサポートいたします。
							</p>
							<div class="c-info1">
								<h4 class="c-info1__title">このような法人様におすすめ</h4>
								<ul class="c-info1__list">
									<li>介護事業の事が全くわからない。</li>
									<li>一人では不安だから一緒に事業計画から安定運営までサポートして欲しい。</li>
									<li>障害福祉サービスを取り組みたい。</li>
									<li>更生保護事業に興味がある。</li>
								</ul>
							</div>
						</div>
						<div class="l-flame1__right">
							<img src="../../assets/image/teamsakata/img-teamsakata1.png" alt="">
						</div>
					</div>
				</div>
			</div>  <!-- /p-teamsakata1 -->

			<div class="p-teamsakata2 u-bg1">
				<div class="l-flame2">
					<div class="l-container">
					<div class="c-title4">
						心理カウンセリング
					</div>
					<h4 class="c-title5"><img src="../../assets/image/teamshien/icon-teamshien.png" alt="">アイリッシュカフェ「心のオアシス」</h4>
					<div class="l-flame1">
						<div class="l-flame1__left">
							<p><span>1人で抱えて、考えているだけでは堂々めぐり・・・</span><br>
							どうしていいのかわからない・・・<br>
							落ち込んでいく・・・</p>
							<div class="box-image-teamsakata">
								<a href="#" title=""><img src="../../assets/image/teamsakata/img-box-teamsakata.png" alt=""></a>
							</div>
						</div>
						<div class="l-flame1__right">
							<img src="../../assets/image/teamsakata/img-teamsakata2.gif" alt="">
						</div>
					</div>
				</div>
				</div>
			</div><!-- /p-teamsakata2 -->

			<div class="p-teamsakata3">
				<div class="l-flame2">
					<div class="l-container">

					<div class="c-title4">
						お問い合わせ先
					</div>

					<dl class="c-table1">
						<dt>電話番号</dt>
						<dd>03-3323-2495</dd>
						<dt>メールアドレス</dt>
						<dd>team-sakata@w6.dion.ne.jp</dd>
					</dl>
				</div>
				</div>
			</div><!-- /p-teamsakata3 -->

			<div class="p-teamsakata4 u-bg1">
				<div class="l-flame2">
					<div class="l-container">
					<div class="c-title4">
						求人案内
					</div>
					<div class="c-text1">
						<p>テキストを入力することができます。テキストを入力することができます。テキストを入力することができます。テキストを入力することができます。テキストを入力することができます。テキストを入力することができます。テキストを入力することができます。テキストを入力することができます。テキストを入力することができます。テキストを入力することができます。テキストを入力することができます。テキストを入力することができます。テキストを入力することができます。テキストを入力することができます。テキストを入力することができます。テキストを入力することができます。テキストを入力することができます。テキストを入力することができます。テキストを入力することができます。テキストを入力することができます。テキストを入力することができます。テキストを入力することができます。</p>
					</div>
				</div>
				</div>
			</div><!-- /p-teamsakata4 -->

			<div class="p-teamsakata5">
				<div class="l-flame2">
					<div class="l-container">
					<div class="c-title4">
						アクセスマップ
					</div>
					<div class="c-mapBlock">
						<div class="c-map1">
							<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3241.4152130342773!2d139.6573658155701!3d35.66677693837128!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x6018f30e11f17561%3A0x9bb452e4b9254772!2s1+Chome-13-9+Hanegi%2C+Setagaya-ku%2C+T%C5%8Dky%C5%8D-to+156-0042%2C+Japan!5e0!3m2!1sen!2s!4v1503037614438" frameborder="0" style="border:0" allowfullscreen=""></iframe>
						</div>
						<a href="https://maps.google.com/maps?ll=35.666773,139.659555&amp;z=16&amp;t=m&amp;hl=ja&amp;gl=US&amp;mapclient=embed&amp;q=%E3%80%92156-0042%20%E6%9D%B1%E4%BA%AC%E9%83%BD%E4%B8%96%E7%94%B0%E8%B0%B7%E5%8C%BA%E7%BE%BD%E6%A0%B9%E6%9C%A8%EF%BC%91%E4%B8%81%E7%9B%AE%EF%BC%91%EF%BC%93%E2%88%92%EF%BC%99%20%E6%97%A5%E6%9C%AC" target="_blank" class="c-map1-detail">詳しい地図を見る</a>
						<div class="c-list1">
							<p><img src="../../assets/image/common/icon-car.png" alt="">京王線「代田橋」駅　徒歩6分</p>
							<p><img src="../../assets/image/common/icon-car.png" alt="">京王井の頭線「新代田」駅　徒歩9分</p>
							<p><img src="../../assets/image/common/icon-car.png" alt="">京王井の頭線「東松原」駅   徒歩9分　</p>
						</div>
						<p class="map-note">※家庭料理のおざわが一階にあります。</p>
					</div>
				</div>
				</div>
			</div><!-- /p-teamsakata5 -->
		
	</div>

<?php include($_SERVER['DOCUMENT_ROOT'] . '/assets/include/footer.php'); ?>
