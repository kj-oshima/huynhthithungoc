<?php include($_SERVER['DOCUMENT_ROOT'] . '/assets/include/header1.php'); ?>
<title>T.E.G</title>
<?php include($_SERVER['DOCUMENT_ROOT'] . '/assets/include/header2.php'); ?>

	<div class="p-top">
		<div class="p-top1">
			<div class="c-mainvisual">
				<img src="../../assets/image/common/img-index.png" alt="">
			</div>
		</div> <!-- /p-top1 -->

		<div class="p-top2 u-bg1">
			<div class="l-container">
				<div class="c-title100">
					<div class="c-title1">
						お知らせ
						<span>News</span>
					</div>
					<button class="c-btn1">
					<a href="#">Facebook</a>
					</button>
				</div>

				<ul class="c-list2">
					<li>
						<p class="c-list2__date">2017/11/25 </p>
						<p class="c-list2__text">イベント情報ページを更新しました。</p>
					</li>
					<li>
						<p class="c-list2__date">2017/10/20</p>
						<p class="c-list2__text">イベント情報ページを更新しました。</p>
					</li>
					<li>
						<p class="c-list2__date">2017/09/25 </p>
						<p class="c-list2__text">ホームページを開設いたしました。今後とも、チームエガオグループをよろしくお願いいたします。</p>
					</li>
				</ul>
			</div>
		</div> <!-- /p-top2 -->

		<div class="p-top3">
			<div class="l-container">
				<div class="c-title1">
					チームエガオグループ（T.E.G Team Egao Group）
					<span>About</span>
					
				</div>

				<div class="c-nav2">
					<p><a href="#"><img src="../../assets/image/top/img-top1.png" alt=""></a></p>
					<p><a href="#"><img src="../../assets/image/top/img-top2.png" alt=""></a></p>
					<p><a href="#"><img src="../../assets/image/top/img-top3.png" alt=""></a></p>
					<p><a href="#"><img src="../../assets/image/top/img-top4.png" alt=""></a></p>
				</div>
			</div>	
		</div> <!-- /p-top3 -->

		<div class="p-top4 u-bg2">
			<div class="l-container">
				<div class="c-info3">
					<div class="c-info3__left">
						<p class="c-info3__left-text1">チームエガオグループで一緒に働きませんか？</p>
						<p class="c-info3__left-text2">ご興味のある方はお電話にてご連絡ください。</p>
						<img src="../../assets/image/common/img-info3.png" alt="">
					</div>
					<div class="c-info3__right">
						<ul>
							<li><a href="#"><span><img src="../../assets/image/top/icon-top1.png" alt=""></span>株式会社team sakata</a></li>
							<li><a href="#"><span><img src="../../assets/image/top/icon-top2.png" alt=""></span>一般社団法人SHIEN</a></li>
							<li><a href="#"><span><img src="../../assets/image/top/icon-top3.png" alt=""></span>一般社団法人team shien</a></li>
							<li><a href="#"><span><img src="../../assets/image/top/icon-top4.png" alt=""></span>NPO法人 ライフサポートさくら</a></li>
						</ul>
					</div>
				</div>
			</div>
		</div> <!-- /p-top4 -->

	</div>


<?php include($_SERVER['DOCUMENT_ROOT'] . '/assets/include/footer.php'); ?>
